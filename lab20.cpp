﻿// lab20.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

template <typename T>
const T& min (const T& a, const T& b)
{
    return (a < b) ? a : b;
}

int main()
{
    int i = min (4, 8);
    std::cout << i << '\n';

    double d = min (7.56, 21.434);
    std::cout << d << '\n';

    char ch = min('b', '9');
    std::cout << ch << '\n';

    return 0;
}